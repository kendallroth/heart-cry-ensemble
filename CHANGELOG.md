# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.0 - 2022-07-07]
### Added
- Streaming platform links/icons
- Music section to About page

## [1.3.0 - 2022-03-21]
### Added
- Conductor field for seasons

### Updated
- Include conductor in member counts/seasons

## [1.2.1] - 2022-03-16
### Fixed
- Replaced `node-sass` (deprecated) with `dart-sass`

## [1.1.0] - 2020-10-13
### Changed
- Disabled cancelled programs and removed from counts

## [1.0.0] - 2020-06-11
### Added
- Site layout and structure (header, footer, content)
- Initial landing page
- "About Us" page with minor content and member list
- "Members" page with member bios
- "Seasons" page and "Season Details" pages
